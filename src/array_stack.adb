procedure Array_Stack is

   generic
      type Element_Type is private;
      Size : Positive;
   package Generic_Array_Stack is
      type Stack is private;

      Underflow : exception;
      Overflow : exception;

      procedure Push(S : in out Stack; Item : Element_Type);
      function Pop(S : in out Stack) return Element_Type;
   private
      type Item_Array is array(1..Size) of Element_Type;
      type Stack is record
         Index : Integer range 0..Size := 0;
         Values : Item_Array;
      end record;
   end Generic_Array_Stack;

   package body Generic_Array_Stack is
      procedure Push(S : in out Stack; Item : Element_Type) is
      begin
         if S.Index = Size then
            raise Overflow;
         end if;
         S.Index := S.Index + 1;
         S.Values(S.Index) := Item;
      end Push;

      function Pop(S : in out Stack) return Element_Type is
         Item : Element_Type;
      begin
         if S.Index = 0 then
            raise Underflow;
         end if;
         Item := S.Values(S.Index);
         S.Index := S.Index-1;
         return Item;
      end Pop;
   end Generic_Array_Stack;

   package Number_Stack is new Generic_Array_Stack(Integer, 100);
   Numbers : Number_Stack.Stack;
   Value : Integer;
   use Number_Stack;
begin
   for I in 1..50 loop
      Push(Numbers, I);
   end loop;

   for I in 1..50 loop
      Value := Pop(Numbers);
   end loop;
end Array_Stack;
