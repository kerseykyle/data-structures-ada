with Ada.Text_IO;
with Ada.Unchecked_Deallocation;

procedure Linked_List is
   type Node;
   type Node_Ptr is access Node;

   type Node is record
      Value : Integer;
      Next  : Node_Ptr;
   end record;

   procedure Free is new Ada.Unchecked_Deallocation (Node, Node_Ptr);

   procedure Append_Item (Value : Integer; Head_Ptr : in out Node_Ptr) is
   begin
      Head_Ptr := new Node'(Value, Head_Ptr);
   end Append_Item;

   procedure Reverse_List (Head_Ptr : in out Node_Ptr) is
      Current : Node_Ptr := Head_Ptr;
      Prev    : Node_Ptr;
      Next    : Node_Ptr;
   begin
      while Current /= null loop
         Next         := Current.Next;
         Current.Next := Prev;
         Prev         := Current;
         Current      := Next;
      end loop;
      Head_Ptr := Prev;
   end Reverse_List;

   procedure Display_List (Head_Ptr : Node_Ptr) is
      Current_Node : Node_Ptr := Head_Ptr;
   begin
      while Current_Node /= null loop
         Ada.Text_IO.Put_Line (Integer'Image (Current_Node.Value));
         Current_Node := Current_Node.Next;
      end loop;
   end Display_List;

   procedure Free_List (Head_Ptr : in out Node_Ptr) is
      Current : Node_Ptr;
   begin
      while Head_Ptr /= null loop
         Current  := Head_Ptr;
         Head_Ptr := Head_Ptr.Next;
         Free (Current);
      end loop;
   end Free_List;

   Head : Node_Ptr;
begin
   Append_Item (1, Head);
   Append_Item (2, Head);
   Append_Item (3, Head);
   Append_Item (4, Head);
   Append_Item (5, Head);
   Append_Item (6, Head);

   Display_List (Head);

   Reverse_List (Head);
   Ada.Text_IO.Put_Line ("reversed:");
   Display_List (Head);

   Free_List (Head);
end Linked_List;
