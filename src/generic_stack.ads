with Ada.Unchecked_Deallocation;

generic
   type Element_Type is private;
package Generic_Stack is
   type Stack is private;
   procedure Push (S : in out Stack; Item : Element_Type);
   function Pop (S : in out Stack) return Element_Type;
   function Empty(S: Stack) return Boolean;
   procedure Free (S : in out Stack);
   Empty_Stack : exception;
private
   type Node;
   type Stack is access Node;
   type Node is record
      Value : Element_Type;
      Next  : Stack;
   end record;
   procedure Free_Node is new Ada.Unchecked_Deallocation (Node, Stack);
end Generic_Stack;
