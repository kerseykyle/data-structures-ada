with Ada.Unchecked_Deallocation;
with Ada.Text_IO;

procedure Linked_Stack is
   generic
      type Element_Type is private;
   package Generic_Stack is
      type Stack is private;
      procedure Push (S : in out Stack; Item : Element_Type);
      function Pop (S : in out Stack) return Element_Type;
      procedure Free (S : in out Stack);
      Empty_Stack : exception;
   private
      type Node;
      type Stack is access Node;
      type Node is record
         Value : Element_Type;
         Next  : Stack;
      end record;
      procedure Free_Node is new Ada.Unchecked_Deallocation (Node, Stack);
   end Generic_Stack;

   package body Generic_Stack is
      procedure Push (S : in out Stack; Item : Element_Type) is
      begin
         S := new Node'(Item, S);
      end Push;

      function Pop (S : in out Stack) return Element_Type is
         Head   : Stack := S;
         Result : Element_Type;
      begin
         if Head = null then
            raise Empty_Stack;
         end if;
         Result := Head.Value;
         S      := Head.Next;
         Free_Node (Head);
         return Result;
      end Pop;

      procedure Free (S : in out Stack) is
         Current : Stack;
      begin
         while S /= null loop
            Current := S;
            S       := S.Next;
            Free_Node (Current);
         end loop;
      end Free;
   end Generic_Stack;

   package Number_Stack is new Generic_Stack(Integer);
   Number_Items : Number_Stack.Stack;
begin
   Number_Stack.Push(Number_Items, 1);
   Number_Stack.Push(Number_Items, 2);
   Number_Stack.Push(Number_Items, 3);
   Ada.Text_IO.Put_Line(Integer'Image(Number_Stack.Pop(Number_Items)));
   Ada.Text_IO.Put_Line(Integer'Image(Number_Stack.Pop(Number_Items)));
   Ada.Text_IO.Put_Line(Integer'Image(Number_Stack.Pop(Number_Items)));
   Number_Stack.Free(Number_Items);
end Linked_Stack;
