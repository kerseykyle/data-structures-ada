with Generic_Stack;
with Ada.Text_IO;

procedure Infix_Expression_Eval is
   function Eval (expr : String) return Float is
      Expression : String := Expr & " ";
      Result : Float := 0.0;
      type Token_Type is (Operator_Token, Number_Token, Other_Token);
      type Substring_Range is record
         Item_Type   : Token_Type := Other_Token;
         Operator : Character := ' ';
         Start_index : Integer;
         End_Index   : Integer;
      end record;

      package Substring_Stack is new Generic_Stack (Substring_Range);
      subtype Numbers is Character range '0' .. '9';
      Tokens : Substring_Stack.Stack;
      Item   : Substring_Range;
      use Substring_Stack;
      Index : Integer := 1;
   begin
      while Index < Expression'Length loop
         if Expression(Index) in '+' | '-' | '*' | '/' | '(' | ')' then
            Item.Item_Type := Operator_Token;
            Item.Operator := Expression(Index);
            Item.Start_Index := Index;
            Item.End_Index := Index;
            Push(Tokens, item);
            Index := Index + 1;

         elsif Expression(Index) in Numbers then
            Item.Item_Type := Number_Token;
            Item.Start_Index := Index;
            Item.End_Index := Index;
            while Index < Expression'Length and Expression(Index) in Numbers | '.' loop
               Item.End_Index := Index;
               Index := Index + 1;
            end loop;
            Push(Tokens, Item);
         else
            Index := Index + 1;
         end if;
      end loop;

      while not Empty(Tokens) loop
         Item := Pop(Tokens);
         Ada.Text_IO.Put_Line(Expression(Item.Start_Index..Item.End_Index));
      end loop;

      return Result;
   end Eval;
   -- 5 * ( 6 + 2 ) - 12 / 4
   Result : Float;
begin
   Result := Eval("(5*(6+2)-12/4)*100.0");
end Infix_Expression_Eval;
