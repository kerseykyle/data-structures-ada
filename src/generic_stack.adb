package body Generic_Stack is
   procedure Push (S : in out Stack; Item : Element_Type) is
   begin
      S := new Node'(Item, S);
   end Push;

   function Pop (S : in out Stack) return Element_Type is
      Head   : Stack := S;
      Result : Element_Type;
   begin
      if Head = null then
         raise Empty_Stack;
      end if;
      Result := Head.Value;
      S      := Head.Next;
      Free_Node (Head);
      return Result;
   end Pop;

   function Empty(S: Stack) return Boolean is
   begin
      return S = null;
   end Empty;

   function Top (S : Stack) return Element_Type is
      Head   : Stack := S;
   begin
      if Head = null then
         raise Empty_Stack;
      end if;
      return Head.Value;
   end Top;

   procedure Free (S : in out Stack) is
      Current : Stack;
   begin
      while S /= null loop
         Current := S;
         S       := S.Next;
         Free_Node (Current);
      end loop;
   end Free;
end Generic_Stack;
