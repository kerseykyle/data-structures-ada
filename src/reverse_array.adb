with Ada.Integer_Text_IO;
with Ada.Text_IO;
procedure Reverse_Array is
   package IIO renames Ada.Integer_Text_IO;
   type Number_Array is array(Integer range <>) of Integer;

   generic
      type Elem_Type is private;
      type Elem_Array is array (Integer range <>) of Elem_Type;
   procedure Reverse_Items(Items : in Elem_Array; Result: out Elem_Array);

   procedure Reverse_Items(Items : in Elem_Array; Result: out Elem_Array) is
      Temp : Elem_Type;
   begin
      for I in Items'Range loop
         Result((Items'Last-I)+1) := Items(I);
      end loop;
   end Reverse_Items;

   procedure Display_Numbers(Items : Number_Array) is
   begin
      for Value of Items loop
         IIO.Put(Item => Value, Width => 4);
      end loop;
      Ada.Text_IO.New_Line;
   end Display_Numbers;

   procedure Reverse_Numbers is new Reverse_Items(Integer, Number_Array);

   subtype Value_Array is Number_Array(1..15);
   Values : Value_Array;
   Reversed_Values : Number_Array(1..15);
begin
   Values := (98, 67, 30, 65, 37, 60, 2, 0, 34, 21, 8, 46, 98, 61, 4);
   Ada.Text_IO.Put_Line("values:");
   Display_Numbers(Values);

   Reverse_Numbers(Values, Reversed_Values);

   Ada.Text_IO.Put_Line("reversed values:");
   Display_Numbers(Reversed_Values);
end Reverse_Array;
